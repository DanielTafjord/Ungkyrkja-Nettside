<!-- Footer -->
<footer>
	<div id="copyright">
    	<ul>
    		<li>&copy; Ungkyrkja Bryne</li>
    	</ul>
	</div>
	<div id="footer-links">
    	<ul>
    		<li><a href="index.php">Heim</a></li>
    		<li><a href="program.php">Program</a></li>
        <li><a href="kontakt.php">Kontakt</a></li>
    		<li><a href="admin.php">Admin</a></li>
    	</ul>
	</div>
</footer>
